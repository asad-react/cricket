import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import ContactList from '../components/contact-list';
import { ContactContext } from '../context/contact-context';
import { FlashMessage, flashErrorMessage } from '../components/flash-message';
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import TopHeader from "../components/top-header";
const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as='h1'
      content='Imagine-a-Company'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as='h2'
      content='Do whatever you want when you want to.'
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
    <Button primary size='huge'>
      Get Started
      <Icon name='right arrow' />
    </Button>
  </Container>
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}
const ContactListPage = () => {
  const [state, dispatch] = useContext(ContactContext);
  const [fixed, setfixed] = useState(false);
  const hideFixedMenu = () => setfixed(false );
  const showFixedMenu = () => setfixed(true);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:3030/contacts');
        dispatch({
          type: 'FETCH_CONTACTS',
          payload: response.data.data || response.data, // in case pagination is disabled
        });
      } catch (error) {
        flashErrorMessage(dispatch, error);
      }
    };
    fetchData();
  }, [dispatch]);

  return (
    // <div>
    //   <h1>List of Contacts</h1>
    //   {state.message.content && <FlashMessage message={state.message} />}
    //   <ContactList contacts={state.contacts} />
    // </div>
    <div>
    <Visibility
    once={false}
    onBottomPassed={showFixedMenu}
    onBottomPassedReverse={hideFixedMenu}
  >
    <Segment

    >
       <TopHeader fixed={true}/>

    </Segment>
  </Visibility>
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container stackable verticalAlign='middle'>
        <Grid.Row>
          <Grid.Column >
            <Header as='h3' style={{ fontSize: '2em' }}>
            <h1>List of Contacts</h1>
            </Header>
            <p style={{ fontSize: '1.33em' }}>
            {state.message.content && <FlashMessage message={state.message} />}
            </p>
            <ContactList contacts={state.contacts} />
          </Grid.Column>

        </Grid.Row>

      </Grid>
    </Segment>






    </div>
  );
}

export default ContactListPage;
